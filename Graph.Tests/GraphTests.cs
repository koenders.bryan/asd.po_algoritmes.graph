using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Graph.Tests
{
    public class GraphTests
    {
        [Fact]
        public void Registering_A_Vertex_In_The_Graph_Should_Succeed()
        {
            //Arrange
            var graph = new DirectedGraph();

            //Act
            graph.AddVertex(new Vertex<string>
            {
                Value = "Vertex 1"
            });

            //Assert
            Assert.True(graph.Vertices.Count > 0);
        }

        [Fact]
        public void Registering_An_Adjacent_Vertex_Should_Succeed()
        {
            //Arrange
            var ver1 = new Vertex<string>
            {
                Value = "Vertex 1"
            };

            var ver2 = new Vertex<string>
            {
                Value = "Vertex 2"
            };

            //Act
            ver1.AddEdge(ver2, 1);

            //Assert
            Assert.True(ver1.Edges.FirstOrDefault(v => v.AdjecentVertex == ver2) != null);
        }

        [Fact]
        public void The_Calculated_Sum_Weight_Should_Equal_The_Actual_Weight()
        {
            //Arrange
            var graph = new DirectedGraph();
            var dijkstra = new DijkstraShortestPathFinder<string>();

            var ver1 = new Vertex<string>
            {
                Value = "Vertex 1"
            };

            var ver2 = new Vertex<string>
            {
                Value = "Vertex 2"
            };

            var ver3 = new Vertex<string>
            {
                Value = "Vertex 2"
            };

            //Act
            graph.AddVertex(ver1);
            graph.AddVertex(ver2);
            graph.AddVertex(ver3);

            ver1.AddEdge(ver2, 10);
            ver2.AddEdge(ver3, 3);

            dijkstra.GetPath(graph.Vertices, ver1, ver3);

            //Assert
            Assert.True(ver3.SumWeight == 13);
        }

        [Fact]
        public void The_Weight_Of_An_Edge_Cannot_Be_Zero_Or_less()
        {
            //Arrange
            var ver1 = new Vertex<string>
            {
                Value = "Vertex 1"
            };

            var ver2 = new Vertex<string>
            {
                Value = "Vertex 2"
            };

            //Act
            //Assert
            Assert.Throws<InvalidOperationException>(() => ver2.AddEdge(ver2, -1));
        }

        [Fact]
        public void Dijksta_Shortest_Path_Returns_Actual_Shortest_Path()
        {
            //Arrange
            var graph = new DirectedGraph();
            var dijkstra = new DijkstraShortestPathFinder<string>();

            var ver1 = new Vertex<string>()
            {
                Value = "V1"
            };

            var ver2 = new Vertex<string>()
            {
                Value = "V2"
            };

            var ver3 = new Vertex<string>()
            {
                Value = "V3"
            };

            //Act
            graph.AddVertex(ver1);
            graph.AddVertex(ver2);
            graph.AddVertex(ver3);

            ver1.AddEdge(ver2, 3);
            ver1.AddEdge(ver3, 5);
            ver2.AddEdge(ver3, 1);

            var actualPath = new LinkedList<Vertex<string>>(new List<Vertex<string>> { ver1, ver2, ver3 });
            var path = dijkstra.GetPath(graph.Vertices, ver1, ver3);

            //Assert
            Assert.True(path.SequenceEqual(actualPath));
        }
    }
}

