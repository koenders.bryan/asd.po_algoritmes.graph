﻿using Graph.Edges;
using System;
using System.Collections.Generic;

namespace Graph
{
    public interface IVertex
    {
        List<IEdge> Edges { get; set; }
    }

    public class Vertex<T> : IVertex
    {
        public Vertex()
        {
            Edges = new List<IEdge>();
        }

        public T Value { get; set; }

        public List<IEdge> Edges { get; set; }

        public bool IsVisited { get; set; }

        public IVertex PreviousNode { get; set; }

        public int SumWeight { get; set; }

        public void AddEdge(IVertex adjacentNode, int edgeWeight)
        {
            if(edgeWeight <= 0)
            {
                throw new InvalidOperationException("The weight cannot be zero or less");
            }

            Edges.Add(new WeightedEdge(adjacentNode, edgeWeight));
        }
    }
}
