﻿namespace Graph
{
    class Program
    {
        static void Main(string[] args)
        {
            var graph = new DirectedGraph();

            var node1 = new Vertex<string>()
            {
                Value = "V1"
            };

            var node2 = new Vertex<string>()
            {
                Value = "V2"
            };

            var node3 = new Vertex<string>()
            {
                Value = "V3"
            };

            var node4 = new Vertex<string>()
            {
                Value = "V4"
            };

            var node5 = new Vertex<string>()
            {
                Value = "V5"
            };

            var node6 = new Vertex<string>()
            {
                Value = "V6"
            };

            node1.AddEdge(node2, 3);
            node1.AddEdge(node3, 5);
            node2.AddEdge(node3, 1);
            node3.AddEdge(node4, 2);
            node3.AddEdge(node6, 1);
            node4.AddEdge(node5, 1);

            graph.AddVertex(node1);
            graph.AddVertex(node2);
            graph.AddVertex(node3);
            graph.AddVertex(node4);
            graph.AddVertex(node5);
            graph.AddVertex(node6);

            var test = graph.GetShortestPath(new DijkstraShortestPathFinder<string>(), node1, node5);
        }
    }
}