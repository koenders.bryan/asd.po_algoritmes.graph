﻿using System.Collections.Generic;

namespace Graph
{
    public class DirectedGraph
    {
        ///<summary>
        ///Bevat alle nodes van deze graph.
        ///</summary>
        public List<IVertex> Vertices { get; private set; }

        public DirectedGraph()
        {
            Vertices = new List<IVertex>();
        }

        public void AddVertex(IVertex vertex)
        {
            Vertices.Add(vertex);
        }

        public LinkedList<IVertex> GetShortestPath(IShortestPathFinder shortestPathFinder, IVertex source, IVertex destination)
        {
            return shortestPathFinder.GetPath(Vertices, source, destination);
        }
    }
}
