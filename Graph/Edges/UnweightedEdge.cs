﻿namespace Graph.Edges
{
    public class UnweightedEdge : IEdge
    {
        public IVertex AdjecentVertex { get; set; }

        public int Weight => 1;

        public UnweightedEdge(IVertex adjacentNode)
        {
            AdjecentVertex = adjacentNode;
        }
    }
}
