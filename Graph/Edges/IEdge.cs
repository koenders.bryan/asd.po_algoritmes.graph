﻿namespace Graph.Edges
{
    public interface IEdge
    {
        IVertex AdjecentVertex { get; set; }

        int Weight { get; }
    }
}
