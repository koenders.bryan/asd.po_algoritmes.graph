﻿namespace Graph.Edges
{
    public class WeightedEdge : IEdge
    {
        public IVertex AdjecentVertex { get; set; }

        public int Weight { get; set; }

        public WeightedEdge(IVertex adjacentNode, int weight = int.MaxValue)
        {
            Weight = weight;
            AdjecentVertex = adjacentNode;
        }
    }
}
