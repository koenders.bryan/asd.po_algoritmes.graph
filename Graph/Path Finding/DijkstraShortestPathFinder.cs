﻿using System.Collections.Generic;
using System.Linq;

namespace Graph
{
    public class DijkstraShortestPathFinder<T> : IShortestPathFinder
    {
        public LinkedList<IVertex> GetPath(List<IVertex> nodes, IVertex source, IVertex destination)
        {
            VisitVertex((Vertex<T>)source);

            return BuildRoute(new LinkedList<IVertex>(), source, destination);
        }

        public void VisitVertex(Vertex<T> vertex)
        {
            if (vertex == null)
            {
                return;
            }                

            //skip, already visited
            if (vertex.IsVisited)
            {
                return;
            }                

            //mark vertex as visited
            vertex.IsVisited = true;

            var destinations = new List<Vertex<T>>();

            foreach (var adjEdge in vertex.Edges)
            {
                var adjVertex = (Vertex<T>)adjEdge.AdjecentVertex;

                //sum the weight, update if the current weight is less than the existing weight
                var summedWeight = adjEdge.Weight + vertex.SumWeight;

                if (adjVertex.SumWeight == 0 || adjVertex.SumWeight > summedWeight)
                {
                    //set properties on adjacent vertex when sumweight is greater than the current summed weight
                    adjVertex.PreviousNode = vertex;
                    adjVertex.SumWeight = summedWeight; 
                }

                //add adjacent vertex to the to visit collection
                destinations.Add(adjVertex);
            }

            //visit all registered vertices in order of the sumweight
            foreach (var destinationVertex in destinations.OrderBy(v => v.SumWeight))
            {
                //visit next vertex
                VisitVertex(destinationVertex);
            }
        }


        private LinkedList<IVertex> BuildRoute(LinkedList<IVertex> nodes, IVertex source, IVertex destination)
        {
            if (destination == null || source == destination)
            {
                nodes.AddLast(destination);
                return nodes;
            }

            BuildRoute(nodes, source, ((Vertex<T>)destination).PreviousNode);
            nodes.AddLast(destination);

            return nodes;
        }
    }
}
