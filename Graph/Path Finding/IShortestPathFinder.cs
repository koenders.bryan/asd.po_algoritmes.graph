﻿using System.Collections.Generic;

namespace Graph
{
    public interface IShortestPathFinder
    {
        LinkedList<IVertex> GetPath(List<IVertex> nodes, IVertex source, IVertex destination);
    }
}